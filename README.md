# Basic Structure

1. Clone this repository to your exercises folder
2. Create a branch with the following naming convention: `<your_name>/solution` (replace `<your_name>` with your actual name i.e. for itamar it would be `itamar/solution`)
3. Please follow all the exercises below. After you solve each exercise, **DON'T FORGET TO MAKE A COMMIT**.
4. When you are done solving all of the exercises push your branch up to github, create a pull request, and assign Itamar and another person in the class to review it.
5. Review your team mate's work by commenting on their pull request, pay close attention to:

- Make sure there is one or more commits per exercise (at least six commits)
- Proper indentation of HTML elements
- HTML validity (by using the [w3c validation service](https://validator.w3.org/#validate_by_upload))
- Correctness of each assignment (according to your idea of the solution)

## Exercise I:

- Create a file named `<your_name>_solution.html` (replace `<your_name>` with your actual name i.e. for Itamar it would be `itamar_solution.html`)
- Create a basic html structure as shown in the following image (please create the file by typing):
  ![](/assets/basic_structure.png)
- Change the name in the author meta to your name

---

## Exercise II:

- For each of the lines in the `<body>`, wrap it in the appropriate heading

---

## Exercise III:

- For each heading add a **hover tooltip** that displays which element it is

---

## Exercise IV:

- Add a comment to explain each html element in the document

---

## Exercise V:

- For each heading add a paragraph below it, with any text you would like, and add a comment to the first `<p>` tag explain what it is.

---

## Exercise VI:

- Add the text from the following [file](/assets/logo.txt) into your html page so that it looks **exactly** like it does in the file.

---

## Bonus Tasks:

If your have already created a pull request and reviewed at least one of your team mates pull requests, create a new branch called `<your_name>/homepage` (replace `<your_name>` with your actual name i.e. for Itamar it would be `itamar/homepage`), and follow the tasks below.

> **Important**: Please create a commit for each task

- [ ] Create a new file `index.html`
- [ ] Add the basic html structure to that file
- [ ] The title of the tab should be "Basic HTML Worksheet Solutions" when the file is opened in the browser
- [ ] Add one heading in the body tag with the same text as above "Basic HTML Worksheet Solutions"
- [ ] Add an unordered list to the body: 
    - The unordered list should contain as many list items as members of the team.
    - Each list item should contain a link to a solution file with the href set to `<member_name>_solution.html` (replace `<member_name>` with the name of a team member i.e. for Itamar it would be `itamar_solution.html`) 
